# OpenML dataset: Pollen-Luxembourg-1992-2018

https://www.openml.org/d/43648

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Daily pollen concentration in luxembourg.
Daily pollen concentration data for 33 pollen types since Jan 1, 1992 in Luxembourg combined with meteo data.

  This is the concentration by m for each type of pollen (graminea, )
  The dataset has been completed with daily meteo data : temperature minimum and maximum in C , precipitation in mm

Data comes from https://data.public.lu/ and http://www.pollen.lu/
**Examples of Critical Thresholds from www.pollen.lu**
Betula, Alnus, Corylus, Quercus, Fagus

Low : 0-10
Medium : 11-50
High : 50 (100)

Gramineae

Low : 0-5
Medium : 6-30
High : 30 (50)

Plantago, Rumex, Chenopodium

Low : 0-3
Medium : 4-15
High : 15

Artemisia

Low : 0-2
Medium : 3-6
High : 6 (20)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43648) of an [OpenML dataset](https://www.openml.org/d/43648). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43648/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43648/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43648/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

